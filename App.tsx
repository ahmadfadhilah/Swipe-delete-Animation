import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { useRef } from 'react';
import { useCallback } from 'react';
import { useState } from 'react';
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';
import Listitem from './components/Listitem';
import { ScrollView } from 'react-native-gesture-handler';

const TITLES = [
  'Record the dismissible tutorial',
  'Leave to the Video',
  'Check github',
  'Hello guys',
  'leave a on the  github repo',
];

interface TaskInterface {
  title: string;
  index: number;
};

const TASKS: TaskInterface[] = TITLES.map((title, index) => ({title, index}));

// const TASKS = [
//   {
//     index: 0,
//     title: 'Record the dismissible tutorial',
//   },
//   { ... }, { ... }, { ... }
// ];

const BACKGROUND_COLOR = '#FAFBFF';

export default function App() {
  const [tasks, setTasks] = useState(TASKS)
  
  const onDismiss = useCallback((task: TaskInterface) => {
    setTasks((tasks) => tasks.filter((item) => item.index !== task.index))
  },[])

  const scrollRef = useRef(null)

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />
      <Text style={styles.title}>Tasks</Text>
    <ScrollView ref={scrollRef} style={{flex: 1}}>
      {tasks.map((task) => (
        <Listitem simultaneousHandlers={scrollRef} key={task.index} task={task} onDismiss={onDismiss}/>
      ))}
    </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
  },
  title: {
    fontSize: 60,
    marginVertical: 40,
    paddingLeft: '5%',
  }
});
